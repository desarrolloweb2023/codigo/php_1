-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema invoices
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema invoices
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `invoices` DEFAULT CHARACTER SET utf8 ;
USE `invoices` ;

-- -----------------------------------------------------
-- Table `invoices`.`customers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `invoices`.`customers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `cuit` VARCHAR(45) NULL DEFAULT NULL,
  `category` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `invoices`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `invoices`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `lastname` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(50) NULL DEFAULT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 29
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `invoices`.`invoices`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `invoices`.`invoices` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `idCustomer` INT(11) NULL DEFAULT NULL,
  `amount` VARCHAR(45) NULL DEFAULT NULL,
  `invoiceDate` DATE NULL DEFAULT NULL,
  `createdDate` DATE NULL DEFAULT NULL,
  `updateDate` VARCHAR(45) NULL DEFAULT NULL,
  `createUser` INT(11) NULL DEFAULT NULL,
  `updateUser` INT(11) NULL DEFAULT NULL,
  `type` VARCHAR(45) NOT NULL,
  `number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_customer_1_idx` (`idCustomer` ASC),
  INDEX `fk_user_idx` (`createUser` ASC),
  CONSTRAINT `fk_customer_1`
    FOREIGN KEY (`idCustomer`)
    REFERENCES `invoices`.`customers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user`
    FOREIGN KEY (`createUser`)
    REFERENCES `invoices`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 50
DEFAULT CHARACTER SET = utf8;