# Patrón de Desarrollo Modelo-Vista-Controlador (MVC)

El patrón de desarrollo Modelo-Vista-Controlador, o MVC, es un enfoque arquitectónico comúnmente utilizado en el desarrollo de aplicaciones web y de software. Su objetivo principal es separar la lógica de la aplicación en tres componentes distintos:

- ### Modelo (Model): 
El modelo representa los datos y la lógica de negocio de la aplicación. Aquí se almacenan los datos y se definen las operaciones que se pueden realizar con esos datos. Por lo general, el modelo no sabe nada sobre cómo se presentarán los datos al usuario.
- ### Vista (View): 
La vista se encarga de la presentación de los datos al usuario. Es la capa encargada de la interfaz de usuario. Muestra los datos del modelo y recibe las interacciones del usuario. La vista no contiene lógica de negocio.
- ### Controlador (Controller): 
El controlador actúa como intermediario entre el modelo y la vista. Responde a las acciones del usuario y coordina las operaciones del modelo. Se encarga de actualizar la vista cuando cambian los datos del modelo y de actualizar el modelo cuando se realizan acciones en la vista.

## Ventajas del patrón MVC:
    - Separación de preocupaciones: Permite una clara separación de las preocupaciones, lo que facilita el mantenimiento y la escalabilidad del código.
    - Reutilización de componentes: Los componentes MVC pueden reutilizarse en diferentes partes de la aplicación, lo que reduce la duplicación de código.
    - Facilita el trabajo en equipo: Dado que el código se divide en componentes distintos, varios desarrolladores pueden trabajar en paralelo en diferentes aspectos de la aplicación.
## Ejemplo de flujo en MVC:
    1.  El usuario interactúa con la vista, por ejemplo, haciendo clic en un botón.
    2.  El controlador recibe la acción del usuario y realiza las operaciones necesarias en el modelo, como actualizar datos.
    3.  El modelo actualiza sus datos y notifica al controlador.
    4.  El controlador actualiza la vista para reflejar los cambios en el modelo.

# Scripts

## configuracion
En estos scripts vamos a definir los datos de conexion a la base de datos como asi el controlador y la accion por defecto

[config/config.php](./config/config.php)
```php
<?php
    return array(
        "driver"   => "mysql", 
        "host"     => "127.0.0.1", //Cambie los parametros segun su configuracion a la base de datos
        "user"     => "user", //Cambie los parametros segun su configuracion a la base de datos
        "pass"     => "pass", //Cambie los parametros segun su configuracion a la base de datos
        "database" => "db", //Cambie los parametros segun su configuracion a la base de datos
        "charset"  => "utf8"

    );
?>
```
[config/global.php](./config/global.php)
```php
<?php
    define("DEFAULT_CONTROLLER", "Index");
    define("DEFAULT_ACTION", "index");
?>
```
## Core
En esta seccion vamos a crear el nucleo de nuestro sistema, teniendo un controlador, una entidad y un modelo base, como un controlador frontal el cual nos va a ayudar con el ruteo a los distintos controladores y vistas, tambien tenemos un HelperView que nos va a ayudar a contruir las rutas mas simple. Tambien vamos a tener el script de conexion a la base de datos.

### Conexion a la base de
[core/Connection.php](core/Connection.php)
```php
<?php
    class Connection {
        //PROPIEDADES DEL OBJETO
        private $driver, $host, $user, $pass, $database, $charset;
        
        // DEFINIMOS EL CONSTRUCTOR EL CUAL NOS PERMITE PODER INSTANCIAR NUESTRAS PROPIEDADES DENTRO DEL MISMO OBJETO
        public function __construct(){
            $db_config = require_once './config/config.php';
            $this->driver = $db_config["driver"];
            $this->host = $db_config["host"];
            $this->user = $db_config["user"];
            $this->pass = $db_config["pass"];
            $this->database = $db_config["database"];
            $this->charset = $db_config["charset"];
        }
        // METODO QUE VAMOS A UTILIZAR PARA REALIZAR LA CONEXION
        public function connect(){
            if($this->driver == "mysql" || $this->driver == null){
                $con = new mysqli($this->host, $this->user, $this->pass, $this->database);
                $con->query("SET NAMES '". $this->charset ."'");

            }
            return $con;
        }
    }

?>
```
### Entidad base
En este script vamos a tener algunas consultas a la base de datos por default.

[core/EntityBase.php](core/EntityBase.php)
```php
<?php
    class EntityBase{
        //ESTA ENTIDAD NOS VA A PERMITIR TENER ALGUNAS QUERIES POR DEFUALT
        private $table, $db, $connection;

        public function __construct($table, $adpater) {
            $this->table =(string) $table;
            $this->db = $adpater;
        }

        public function db() {
            return $this->db;
        }

        public function getAll() {
            $query = $this->db->query("SELECT * FROM $this->table ORDER BY id DESC");

            while($row = $query->fetch_object()) {
                $resultSet[]=$row;
            }

            return $resultSet;
        }

        public function getById($id) {
            $query = $this->db->query("SELECT * FROM $this->table WHERE id=$id");

            if($row = $query->fetch_object()) {
                $resultSet=$row;
            }

            return $resultSet;
        }

        public function getByEmail($email) {
            $query = $this->db->query("SELECT * FROM $this->table WHERE email='".$email."'");

            while($row = $query->fetch_object()) {
                $resultSet[]=$row;
            
            }

            return $resultSet;
        }

        public function getBy($column, $value) {
            $query = $this->db->query("SELECT * FROM $this->table WHERE $column=$value");

            while($row = $query->fetch_object()) {
                $resultSet[]=$row;
            }

            return $resultSet;
        }

        public function deleteById($id){
            $query=$this->db->query("DELETE FROM $this->table WHERE id=$id");
            return $query;
        }
         
        public function deleteBy($column,$value){
            $query=$this->db->query("DELETE FROM $this->table WHERE $column='$value'");
            return $query;
        }
    }
?>
```
### Modelo base
En este modelo base es el que nos va a permitir ejecutar las consultas a la base de datos.

[core/ModelBase.php](core/ModelBase.php)
```php
<?php
class ModelBase extends EntityBase{
    private $table;
     
    public function __construct($table,  $adapter) {
        $this->table = (string) $table;
        parent::__construct($table,  $adapter);
         
    }

    public function runSql($query){
        $query=$this->db()->query($query);
        if($query){
            if($query->num_rows > 1){
                while($row = $query->fetch_object()) {
                   $resultSet[]=$row;
                }
            }elseif($query->num_rows == 1){
                if($row = $query->fetch_object()) {
                    $resultSet=$row;
                }
            }else{
                $resultSet=true;
            }
        }else{
            $resultSet=false;
        }
         
        return $resultSet;
    }
     
     
}
```
### Ayuda con las vistas
Conesta ayuda con las vistas vamos a poder construir obejtos que nos contruyan las url dentro de nuestro codigo de manera mas simple-

[core/HelperViews.php](core/HelperViews.php)
```php
<?php
class HelperViews{
     
    public function url($controller=DEFAULT_CONTROLLER,$action=DEFAULT_ACTION){
        $urlString="index.php?controller=".$controller."&action=".$action;
        return $urlString;
    }
     
    //Helpers para las vistas
}
?>
```
### Controlador base
El controlador base va a ser el que nos vincule todos nuestros scripts anteriores y podamos utilizarlos desde cualquier otro controlador.

[core/ControllerBase.php](core/ControllerBase.php)
```php
<?php
class ControllerBase{
 
    public function __construct() {
        require_once 'Connection.php';
        require_once 'EntityBase.php';
        require_once 'ModelBase.php';
         
        //Incluir todos los modelos
        foreach(glob("models/*.php") as $file){
            require_once $file;
        }
    }
     
/*
* Este método lo que hace es recibir los datos del controlador en forma de array
* los recorre y crea una variable dinámica con el indice asociativo y le da el
* valor que contiene dicha posición del array, luego carga los helpers para las
* vistas y carga la vista que le llega como parámetro. En resumen un método para
* renderizar vistas.
*/
    public function view($view,$data){
        foreach ($data as $id_assoc => $valor) {
            ${$id_assoc}=$valor;
        }
         
        require_once 'core/HelperViews.php';
        $helper=new HelperViews();
     
        require_once 'views/'.$view.'View.php';
    }
     
    public function redirect($controller=DEFAULT_CONTROLLER,$action=DEFAULT_ACTION){
        header("Location:index.php?controller=".$controller."&action=".$action);
    }
     
    //Métodos para los controladores
 
}
?>
```

### Controlador frontal
El controlador frontal va a ser el encargado de recibir los parametros que recibimos en la url y enviarlo al controlador correcto

[core/ControllerFront.func.php](core/ControllerFront.func.php)
```php
<?php
//FUNCIONES PARA EL CONTROLADOR FRONTAL
 
function loadController($controller){
    $controlador=ucwords($controller).'Controller';
    $strFileController='controllers/'.$controlador.'.php';
     
    if(!is_file($strFileController)){
        $strFileController='controllers/'.ucwords(DEFAULT_CONTROLLER).'Controller.php';
        $controlador=ucwords(DEFAULT_CONTROLLER).'Controller';  
    }
    // die(var_dump($strFileController));
    require_once $strFileController;
    $controllerObj=new $controlador();
    return $controllerObj;
}

// Esta funcion nos va a ayudar a cargar la accion del controlador que necesitemos
function loadAction($controllerObj,$action){
    $accion=$action;
    $controllerObj->$accion();
}
// Esta funcion nos ayuda a verificar si no existe la accion en el controlador poder cargar la accion por defecto
function launchAction($controllerObj){
    if(isset($_GET["action"]) && method_exists($controllerObj, $_GET["action"])){
        loadAction($controllerObj, $_GET["action"]);
    }else{
        loadAction($controllerObj, DEFAULT_ACTION);
    }
}
 
?>
```


