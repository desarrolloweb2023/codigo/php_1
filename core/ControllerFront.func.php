<?php
//FUNCIONES PARA EL CONTROLADOR FRONTAL
 
function loadController($controller){
    $controlador=ucwords($controller).'Controller';
    $strFileController='controllers/'.$controlador.'.php';
     
    if(!is_file($strFileController)){
        $strFileController='controllers/'.ucwords(DEFAULT_CONTROLLER).'Controller.php';
        $controlador=ucwords(DEFAULT_CONTROLLER).'Controller';  
        // die(var_dump($strFileController));
    }
    // die(var_dump($strFileController));
    require_once $strFileController;
    $controllerObj=new $controlador();
    //die(var_dump($controllerObj));
    return $controllerObj;
}

// Esta funcion nos va a ayudar a cargar la accion del controlador que necesitemos
function loadAction($controllerObj,$action){
    $accion=$action;
    $controllerObj->$accion();
}
// Esta funcion nos ayuda a verificar si no existe la accion en el controlador poder cargar la accion por defecto
function launchAction($controllerObj){
    if(isset($_GET["action"]) && method_exists($controllerObj, $_GET["action"])){
        loadAction($controllerObj, $_GET["action"]);
    }else{
        loadAction($controllerObj, DEFAULT_ACTION);
    }
}
 
?>