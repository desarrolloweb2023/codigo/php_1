<?php //die(var_dump($hola));?>
<?php include './views/header.php';?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
<div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <!-- Small boxes (Stat box) -->
        <div class="card-header">
          <h3 class="card-tittle"><?php echo $hola ?></h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Saepe beatae vitae molestiae temporibus ab dignissimos nemo sed nulla natus dolorem, culpa modi qui laudantium, sunt accusantium nostrum iure nihil assumenda.</p>
        </div>
        <div class="card-footer">
          <p></p>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include './views/footer.php';?>