$(document).ready(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    $('#login').validate({
        rules: {
          email: {
            required: true,
            email: true
          },
          password: {
            required: true,
            minlength: 5
          },
        },
        messages: {
          email: {
            required: "Por favor ingrese su email",
            email: "Por favor ingrese un email valido"
          },
          password: {
            required: "Por favor ingrese su password",
            minlength: "Su password debe contener como minimo cinco(5) caracteres"
          }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
          error.addClass('invalid-feedback');
          element.closest('.input-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
          $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
          $(element).removeClass('is-invalid');
          $(element).addClass('is-valid');
        },
        submitHandler: function (form) {
          var datos = $(form).serializeArray();
          //console.log(datos);
          //alert( "Form successful submitted!" );
          //form.submit();
          
  
          $.ajax({
            method: $(form).attr('method'),
            data: datos,
            url: 'index.php?controller=Login&action=login',
            dataType: 'json',
            async: false,
            success: function(data) {
              console.log(data);
              var result = data;
              console.log(typeof(result));
              if (result.status == "ok") {
                 Toast.fire({
                  icon: 'success',
                  title: 'Success',
                  text: 'Bienvenid@ '+result.result[0].name+' !!!'
                }).then(function(){
                    window.location = 'index.php?controller=Index&action=index';
                })
                
              } else if(result.status == "passBad") {
                Toast.fire({
                  icon: 'error',
                  title: 'Error',
                  text: 'Password incorrecto'
                })
              } else if(result.status == "emailBad") {
                Toast.fire({
                  icon: 'error',
                  title: 'Error',
                  text: 'Email no existe'
                })
              }
  
              
            }
          })
        }
  
      })
});