
$(document).ready(function() {
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
  $('.select2bs4').select2({
    theme: 'bootstrap4',
    placeholder: 'Seleccione un rol'
  });
    $('#userForm').validate({
      rules: {
        name: {
            required: true,
            minlength: 2,
            maxlength: 30,
        },
        lastname: {
            required: true,
            minlength: 2,
            maxlength: 30,
        },
        email: {
          required: true,
          email: true
        },
        role: {
          required: true
        },
        password: {
          required: true,
          minlength: 5
        },
      },
      messages: {
        name: {
            required: "Por favor ingrese su nombre",
            minlength: "Su nombre debe contener como minimo dos(2) caracteres",
            maxlength: "Su nombre debe contener como maximo treinta(30) caracteres",
        },
        lastname: {
            required: "Por favor ingrese su apellido",
            minlength: "Su apellido debe contener como minimo dos(2) caracteres",
            maxlength: "Su apellido debe contener como maximo treinta(30) caracteres",
        },
        email: {
          required: "Por favor ingrese su email",
          email: "Por favor ingrese un email valido"
        },
        role: { 
          required: "Por favor seleccione un rol"
        },
        password: {
          required: "Por favor ingrese su password",
          minlength: "Su password debe contener como minimo cinco(5) caracteres"
        }
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        

        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Users&action=create',
          dataType: 'json',
          async: false,
          success: function(data) {
            console.log(data);
            var result = data[0];
            console.log(typeof(result[0]));
            if (result[0]) {
               Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'El usuario se registro correctamente'
              }).then(function(){
                  window.location = 'index.php?controller=Users&action=index';
              })
              
            } else {
              Toast.fire({
                icon: 'error',
                title: 'Error',
                text: 'El email ya existe, por favor ingrese otro email'
              })
            }

            //window.location = 'index.php?controller=Users&action=index';
            
          }
        })
      }
    })


    $('#updateUser').validate({
      rules: {
        name: {
            required: true,
            minlength: 2,
            maxlength: 30,
        },
        lastname: {
            required: true,
            minlength: 2,
            maxlength: 30,
        },
        email: {
          required: true,
          email: true
        },
        role: {
          required: true
        },
        password: {
          required: true,
          minlength: 5
        },
      },
      messages: {
        name: {
            required: "Por favor ingrese su nombre",
            minlength: "Su nombre debe contener como minimo dos(2) caracteres",
            maxlength: "Su nombre debe contener como maximo treinta(30) caracteres",
        },
        lastname: {
            required: "Por favor ingrese su apellido",
            minlength: "Su apellido debe contener como minimo dos(2) caracteres",
            maxlength: "Su apellido debe contener como maximo treinta(30) caracteres",
        },
        email: {
          required: "Por favor ingrese su email",
          email: "Por favor ingrese un email valido"
        },
        role: { 
          required: "Por favor seleccione un rol"
        },
        password: {
          required: "Por favor ingrese su password",
          minlength: "Su password debe contener como minimo cinco(5) caracteres"
        }
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        

        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Users&action=update',
          dataType: 'json',
          async: false,
          success: function(data) {
            console.log(data);
            var result = data[0];
            console.log(typeof(result[0]));
            if (result[0]) {
               Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'El usuario se registro correctamente'
              }).then(function(){
                  window.location = 'index.php?controller=Users&action=index';
              })
              
            } else {
              Toast.fire({
                icon: 'error',
                title: 'Error',
                text: 'El email ya existe, por favor ingrese otro email'
              })
            }

            //window.location = 'index.php?controller=Users&action=index';
            
          }
        })
      }
    })

    $(document).on('click','.delete', function(e){
      e.preventDefault();
      var id = $(this).attr('data-id');
      var controller = $(this).attr('data-controller');
      var name = $(this).attr('data-name');
      Swal.fire({
        icon: 'question',
        title: '¿Quiere borrar el registro?',
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if(result.isConfirmed) {
          $.ajax({
            type: 'post',
            dataType: 'json',
            url: "index.php?controller="+controller+"&action=delete",
            data: {id: id},
            success: function(){
              Toast.fire({
                icon: 'success',
                cancelButtonText: 'Cancelar',
                title: 'Se eliminado correctamente el registro ' + name
              }).then(function(){
                window.location = "index.php?controller="+controller+"&action=index";
            })
            }
          })
        }  
      })
    })
    // 
    // 
    // 
    // 
    
    


});
